import pytest
import os
import subprocess
import pathlib
import re
import sys
import xml.etree.ElementTree as ET

#pathlib path.glob()

def pytest_collection(session):
    print("entering pytest_collection", id(session))
    session.qmt_files = {qmt_filename_to_name(str(f.relative_to(session.startpath))): f.relative_to(session.startpath)
     for f in session.startpath.glob("**/*.qmt")}
    print(session.qmt_files)
    #pytest_collectstart(collector=session)

def pytest_itemcollected(item):
    print(item.path, "with deps", [mk.kwargs['after'] for mk in item.iter_markers("order")])

def pytest_collect_file(parent, file_path):
    #print(file_path)
    if file_path.suffix == ".qmt":
        # print(id(parent), parent.hello)
        return QmtFile.from_parent(parent, path=file_path)


class QmtFile(pytest.File):
    def collect(self):
        name=qmt_filename_to_name(str(self.path.relative_to(self.parent.startpath)))
        item = QmtItem.from_parent(self,
         name=name,
         path=self.path)
        item.cls = None # !!! why !?!?!?!?
        #for dep in get_dependency(pkg, self.path):
            #item.add_marker(dep)
        if item.name == "qmtests.something.a":
            item.add_marker(pytest.mark.order(after = str(self.parent.qmt_files["qmtests.something.bug_35347"])+"::qmtests.something.bug_35347"))
        yield item


class QmtItem(pytest.Item):
    def runtest(self):
        print("QmtItem:", self.path)
        # assert "properties" not in str(self.path)s
        # assert False



def qmt_filename_to_name(path):
    """
    convert the relative path to a .qmt/.qms file to the canonical QMTest test
    name.

    For example:

    >>> qmt_filename_to_name('some_suite.qms/sub.qms/mytest.qmt')
    'some_suite.sub.mytest'
    """
    return ".".join(re.sub(r"\.qm[st]$", "", p) for p in path.split(os.path.sep))


#TODO: integrate helper functions to modify names to the needed format and parse files

def fix_test_name(name, pkg):
    """
    Convert the QMTest test name to the name used in CTest.

    >>> fix_test_name('package.bug.123', 'Package')
    'Package.bug.123'

    >>> fix_test_name('Package.Bug.123', 'Package')
    'Package.Bug.123'

    >>> fix_test_name('simple', 'Package')
    'Package.simple'
    """
    return re.sub(r"^((%s|%s)\.)?" % (pkg.lower(), pkg), "%s." % pkg, name)


def find_files(rootdir, ext):
    """
    Find recursively all the files in a directory with a given extension.
    """
    for dirpath, _dirnames, filenames in os.walk(rootdir):
        for filename in filenames:
            if os.path.splitext(filename)[1] == ext:
                yield os.path.join(dirpath, filename)


def parse_xml(path):
    """
    Return the parsed tree, handling exceptions if needed.
    """
    try:
        return ET.parse(path)
    except ET.ParseError as e:
        sys.stderr.write("ERROR: could not parse {}\n{}\n".format(path, e))
        sys.stderr.flush()
        exit(1)


def _pytest_collection_modifyitems(session, config, items):
    """
    Attaches prereqs to items using order markers
    """
    for item in items:
        prereq_list = get_dependency(pkg, item)    
        for prereq in prereq_list:
            item.add_marker(pytest.mark.order(befor = prereq))




def get_dependency(pkg, path):
    """
    Collects prereqs to specific test items
    """
    name = qmt_filename_to_name(os.path.relpath(path))
    name = fix_test_name(name, pkg)
    tests = {}
    tests[name] = path

    prereq_xpath = 'argument[@name="prerequisites"]/set/tuple/text'
    for name, path in tests.items():
        tree = parse_xml(path)
        prereqs = [fix_test_name(el.text, pkg) for el in tree.findall(prereq_xpath)]
        for prereq in prereqs:
            if prereq not in tests:     
                                        
                sys.stderr.write(
                    "ERROR: prerequisite {0} from {1} not found.\n".format(prereq, path)
                )
                sys.stderr.flush()
                exit(1)
        if prereqs:
            return prereqs


