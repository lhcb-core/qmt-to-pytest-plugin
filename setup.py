from setuptools import setup

setup(
  name="qmt-to-pytest-plugin",
  version="0.1.0",
  description="pytest plugin that handles qmt files",
  entry_points={"pytest11": ["pytest-qmtest = lb.pytest.qmtest"]},
)
